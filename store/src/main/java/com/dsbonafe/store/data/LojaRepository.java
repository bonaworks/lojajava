package com.dsbonafe.store.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dsbonafe.store.model.Loja;

@Repository
public interface LojaRepository extends JpaRepository<Loja, Integer>{

	public Loja findByName(String nomeLoja);

}
