package com.dsbonafe.store;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.dsbonafe.store.data.ClienteRepository;
import com.dsbonafe.store.data.LojaRepository;
import com.dsbonafe.store.model.Cliente;
import com.dsbonafe.store.model.Loja;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LojaTest {

	@Autowired
	Loja loja;
	@Autowired
	LojaRepository lojaRepository;
	@Autowired
	ClienteRepository clienteRepository;

	@Test
	public void createLoja() {
		lojaRepository.save(loja);
		assertNotNull(lojaRepository);
	}

	@Test
	public void createCliente() {
		createClienteAndSaveIt();
		assertNotNull(clienteRepository);
	}

	@Test
	public void adicionaClientesNaLoja() {
		loja.getClientes().add(createClienteAndSaveIt());
		lojaRepository.saveAndFlush(loja);
		assertNotNull(lojaRepository);
	}

	private Cliente createClienteAndSaveIt() {
		Cliente cliente = new Cliente();
		clienteRepository.save(cliente);
		return cliente;
	}

}
