package com.dsbonafe.store.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootConfiguration
@ComponentScan(basePackages = { "com.dsbonafe.store" })
public class StoreConfig {

}
