package com.dsbonafe.store.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Loja {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String name;

	@OneToMany
	private List<Cliente> clientes;

	@OneToMany
	private List<Produto> produtos;

	public Loja() {
		this.clientes = new ArrayList<Cliente>();
		this.produtos = new ArrayList<Produto>();
	}

	public List<Cliente> getClientes() {
		return this.clientes;
	}

	public List<Produto> getProdutos() {
		return this.produtos;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
