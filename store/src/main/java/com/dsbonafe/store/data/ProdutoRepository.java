package com.dsbonafe.store.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dsbonafe.store.model.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

	public Produto findByName(String name);
}
