package com.dsbonafe.store.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.hibernate.validator.internal.util.privilegedactions.GetClassLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dsbonafe.store.controller.dtos.ClienteLojaDto;
import com.dsbonafe.store.controller.dtos.ClienteLojaProdutoDto;
import com.dsbonafe.store.controller.dtos.ProdutoLojaDto;
import com.dsbonafe.store.data.ClienteRepository;
import com.dsbonafe.store.data.LojaRepository;
import com.dsbonafe.store.data.ProdutoRepository;
import com.dsbonafe.store.model.Cliente;
import com.dsbonafe.store.model.Loja;
import com.dsbonafe.store.model.Produto;

@RestController
@RequestMapping("/")
@Transactional
public class LojaController {

	@Autowired
	LojaRepository lojaRepository;
	@Autowired
	ClienteRepository clienteRepository;
	@Autowired
	ProdutoRepository produtoRepository;

	@RequestMapping(value = "produtos", method = RequestMethod.GET, produces = "application/json")
	public List<Produto> getProdutos() {
		List<Produto> produtos = produtoRepository.findAll();
		return produtos;
	}

	@GetMapping("clientes")
	public List<Cliente> getClientes() {
		return clienteRepository.findAll();
	}

	@GetMapping("lojas")
	public List<Loja> getLojas() {
		return lojaRepository.findAll();
	}

	@PostMapping("/newClient")
	@ResponseBody
	public Cliente createClient(@RequestBody Cliente cliente) {
		clienteRepository.saveAndFlush(cliente);
		return cliente;
	}

	@PostMapping("/newProduct")
	public Produto createProduto(@RequestBody Produto produto) {
		produtoRepository.saveAndFlush(produto);
		return produto;
	}

	@PostMapping("/newStore")
	@ResponseBody
	public Loja createLoja(@RequestBody Loja loja) {
		lojaRepository.saveAndFlush(loja);
		return loja;
	}

	/**
	 * Toda vez que eu quiser fazer um POST, PUT, DELETE onde eu vá manipular mais do que um objeto do meu MODELO,
	 * então eu devo criar um DTO apenas com os dados destes objetos que eu queira manipular.
	 */
	@PostMapping("/addProductStore")
	@ResponseBody //Para retornar no Body, como JSON, um objeto
	public Loja addProductStore(@RequestBody ProdutoLojaDto produtoLojaDto) {
		Produto produto = produtoRepository.findByName(produtoLojaDto.getNomeDoProduto());
		Loja loja = lojaRepository.findByName(produtoLojaDto.getNomeLoja());
		loja.getProdutos().add(produto);
		lojaRepository.saveAndFlush(loja);
		return loja;
	}

	@PostMapping("/addClientStore")
	@ResponseBody
	public Loja addClientStore(@RequestBody ClienteLojaDto clienteLojaDto) {
		Cliente cliente = clienteRepository.findByCpf(clienteLojaDto.getCpfDoCliente());
		Loja loja = lojaRepository.findByName(clienteLojaDto.getNomeDaLoja());
		loja.getClientes().add(cliente);
		lojaRepository.saveAndFlush(loja);
		return loja;
	}

	@PostMapping("/vendeProduto")
	@ResponseBody
	public Produto vendeProduto(@RequestBody ClienteLojaProdutoDto dto) throws Exception {
		Cliente cliente = clienteRepository.findByCpf(dto.getCpfDoCliente());
		Loja loja = lojaRepository.findByName(dto.getNomeDaLoja());

		// Biblioteca Stream do Java 8
		Produto vendido = loja.getProdutos().stream().filter(p -> p.getName().equals(dto.getNomeDoProduto()))
				.findFirst().get();

		/**
		 * Didaticamente, as linhas 104 e 105 fazem o mesmo que o for abaixo:
		 */

//		Produto vendido = null;
//		for(Produto p : loja.getProdutos()) {
//			if(p.getName().equals(dto.getNomeDoProduto())){
//				vendido = p;
//			}
//		}

		if (cliente.getCash() >= vendido.getPrice()) {
			
			/**
			 * Toda vez que eu modificar um dado de um objeto do meu modelo, tão logo eu o modifique, DEVO ATUALIZá-lo
			 * E SALVÁ-LO NO BANCO DE DADOS. Isso evita TransientObjectException.
			 */

			loja.getProdutos().remove(vendido);
			lojaRepository.saveAndFlush(loja);

			cliente.getProdutos().add(vendido);
			clienteRepository.saveAndFlush(cliente);
		} else {
			double resto =  vendido.getPrice() - cliente.getCash();
			throw new Exception(String.format("Cliente não tem dinheiro suficiente. Faltam: %f" , resto));
		}

		return vendido;
	}
}
