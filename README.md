# EXERCÍCIOS DA SEMANA
1. Exercícios de Compreensão
   - Que significam os termos backend e frontend? Por que separamos uma aplicação nestas duas camadas?
   - Em quantas camadas se divide uma aplicação de backend? Que é o padrão MVC?
   - Que fazemos na camada de modelo?
   - Que fazemos na camada de dados?
   - Que fazemos na camada de controller?
   - Por que não usamos a camada de View do modelo MVC no backend?
   - Que diferencia uma aplicaçao monolítica de uma aplicação em microsserviços?
   - Quais as vantagens de uma aplicação em Microsserviços?
   - Que é o Spring?
   - Por que usamos o Spring Boot?
   - Quais os pacotes do Spring Boot (ou quais suas features) estamos utilizando em nosso projeto?
   - Podemos dizer que nosso projeto é um Serviço Web (Web Service)?
   - Que é o modelo REST? Por que utilizamos esse modelo?
   - Que é o protocolo HTTP?
   - Existem outros modelos que não o REST para tráfego de dados a internet? Qual/Quais?
   - Que é um banco de dados?
   - Que é um banco de dados relacional? Existem outros modelos?
   - No Spring Framework, quais os passos para se conectar com um banco de dados relacional?
   - Qual o pacote do Java nós costumamos usar para nos relacionarmos ao banco de dados relacional?
   - No Hibernate, como convertemos uma classe em uma tabela do banco de dados?
   - Que significa **endpoint**?
   - Em que camada são definidos os _endpoints_ da aplicação?
   - De quais formas podemos acessar um _endpoint_?
   - Que são _annotations_?
   - Que _annotations_ devemos usar para configurarmos uma classe como controller?
   - Que _annotations_ devemos usar para fazermos injeção de dependências? Que significa injeção de dependência?
   - Quais _annotations_ podemos usar para fazer uma requisição de **POST**?
   - Quais são os principais códigos de erro retornados por uma requisição HTTP?
   - Qual a relação do REST com o protocolo HTTP?

2. No projeto, leia atentamente todos os códigos gerados e crie um método para buscar um produto pelo **nome**.

3. Observe que eu criei uma assinatura para um método `findByName(String name)` no `ProdutoRepository`, mas não implementei esse método em lugar algum. Pesquise e me diga como criar pesquisas personalizadas com **SpringBootData**.


## REFERÊNCIAS
- [Monolithic vs. Microsservices](https://medium.com/@raycad.seedotech/monolithic-vs-microservice-architecture-e74bd951fc14)
*[Video: what is MVC?](https://www.youtube.com/watch?v=fI_w0XBo4ZM)
*[Spring e Spring Boot](https://www.devmedia.com.br/spring-boot-simplificando-o-spring/31979)
*[O que é RESTful](https://www.youtube.com/watch?v=7Cbd8WBGrq4)
*[Por que você deve aprender REST?](https://www.youtube.com/watch?v=P6GcoLU0iCo)
*[O que são APIs?](https://becode.com.br/o-que-e-api-rest-e-restful/)
*[O que é REST?](https://www.devmedia.com.br/rest-tutorial/28912)
*[Criando aplicações REST com Spring Boot](https://spring.io/guides/gs/rest-service/)
*[Tudo que precisa saber sobre REST com Spring Boot](https://www.tutorialspoint.com/spring_boot/spring_boot_building_restful_web_services.htm)
